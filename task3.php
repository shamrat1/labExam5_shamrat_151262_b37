<?php
class MyCalculator{
    public $number1 , $number2;

    public function __construct($number1,$number2)
    {
        $this->number1 = $number1;
         $this->number2 = $number2;
    }

    public function addition(){

        return $this->number1 + $this->number2;
    }
    public function subtraction(){
        return $this->number1 - $this->number2;
    }
    public function multiplication(){
        return $this->number1 * $this->number2;
    }
    public function division(){
        if($this->number1 <=0 || $this->number2 <=0){
            echo "division by zero is not supported<br>";
        }
        else{
        return $this->number1 / $this->number2;
        }
    }
}

$myCalc = new MyCalculator(12,12);
echo "Result of Addition :".$myCalc->addition()."<br>";
echo "Result of Multiplication :".$myCalc->multiplication()."<br>";
echo "Result of Subtraction :".$myCalc->subtraction()."<br>";
echo "Result of Division :".$myCalc->division()."<br>";