<?php
$startDate = new DateTime("1981-11-03");
$endDate = new DateTime("2013-09-04");

$difference = $startDate->diff($endDate);

echo "difference between 1981-11-03 & 2013-09-04 is ".$difference->y."years, ".$difference->m."month & ".$difference->d."day";