<?php
class factorial_of_a_number
{

    public $number;

    public function __construct($number)
    {

        if (!is_int($number)) {

            throw new InvalidArgumentException('Not a number or missing argument');

        }
        $this->number = $number;
    }

    public function result()
    {

        $number = $this->number;
        function factorial($fact)
        {
            if ($fact < 2) {
                return 1;
            } else {
                return ($fact * factorial($fact - 1));
            }

        }
        return factorial($number);
    }
}

$newfactorial = New factorial_of_a_number(4);
echo $newfactorial->result();
?>